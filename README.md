# devops-netology
**/.terraform/* - игнорируются все файлы в каталогахвида **/.terraform/* во всех подкаталогах
.tfstate - игнорируются все файлы с расширением .tfstate
*.tfstate.* - игнорируются все временные файлы вида *.tfstate.*
crash.log - игнорируется файл crash.log
*.tfvars - игнорируются все файл с расширением *.tfvars
override.tf - игнорируется файл override.tf
override.tf.json - игнорируется файл override.tf.json
*_override.tf - игнорируются все файлы, имена которых заканчиваются на _override.tf
*_override.tf.json - игнорируются все файлы, имена которых заканчиваются на _override.tf.json
.terraformrc - игнорируется файл .terraformrc
terraform.rc - игноириуется файл terraform.rc
